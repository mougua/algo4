package algo.ch0103;

import java.util.Iterator;

/**
 * @author mougua
 */
public class Stack<E> implements Iterable<E> {
    private Node first;
    private int n;

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        return n;
    }

    public void push(E e) {
        Node oldFirst = first;
        first = new Node();
        first.e = e;
        first.next = oldFirst;
        n++;
    }

    public E pop() {
        E e = first.e;
        first = first.next;
        n--;
        return e;
    }


    private class Node {
        E e;
        Node next;
    }
}
