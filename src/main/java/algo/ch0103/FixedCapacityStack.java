package algo.ch0103;

import java.util.Arrays;

/**
 * @author mougua
 */
public class FixedCapacityStack<T> {
    private T[] a;
    private int n;

    @SuppressWarnings("unchecked")
    public FixedCapacityStack(int cap) {
        a = (T[]) new Object[cap];
    }

    public boolean isEmpty() {
        return n == 0;
    }

    public int size() {
        return n;
    }

    public void push(T item) {
        a[n++] = item;
    }

    public T pop() {
        T t = a[--n];
        a[n] = null;
        return t;
    }

    @Override
    public String toString() {
        return "FixedCapacityStack{" +
                "a=" + Arrays.toString(a) +
                ", N=" + n +
                '}';
    }

}
