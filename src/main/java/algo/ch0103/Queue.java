package algo.ch0103;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author mougua
 */
public class Queue<E> implements Iterable<E> {
    private Node first;
    private Node last;
    private int n;

    @Override
    public Iterator<E> iterator() {
        return new ListIterator();
    }

    void enqueue(E e) {
        Node oldLast = last;
        last = new Node(e, null);
    }

    E dequeue() {
        E e = first.e;
        first = first.next;
        if (isEmpty()) {
            last = null;
        }
        n--;
        return e;
    }


    boolean isEmpty() {
        return first == null;
    }

    int size() {
        return n;
    }

    private class Node {
        E e;
        Node next;
        Node(E e, Node next){
            this.e = e;
            this.next = next;
        }
    }

    private class ListIterator implements Iterator<E>{
        private Node current = first;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public E next() throws NoSuchElementException {
            if (current == null) {
                throw new NoSuchElementException();
            }
            E e = current.e;
            current = current.next;
            return e;
        }

        @Override
        public void remove() {

        }
    }
}
