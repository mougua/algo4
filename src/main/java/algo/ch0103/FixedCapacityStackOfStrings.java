package algo.ch0103;

import java.util.Arrays;

/**
 * @author mougua
 */
public class FixedCapacityStackOfStrings {
    private String[] a;
    private int n;
    public FixedCapacityStackOfStrings(int cap) {
        a = new String[cap];
    }

    public boolean isEmpty() {
        return n == 0;
    }

    public int size() {
        return n;
    }

    public void push(String item) {
        a[n++] = item;
    }

    public String pop() {
        return a[--n];
    }

    @Override
    public String toString() {
        return "FixedCapacityStackOfStrings{" +
                "a=" + Arrays.toString(a) +
                ", N=" + n +
                '}';
    }
}
