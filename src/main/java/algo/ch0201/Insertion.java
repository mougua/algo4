package algo.ch0201;

/**
 * @author mougua
 */
public class Insertion extends SortAlgorithm {
    public static void sort(Comparable[] a) {
        int n = a.length;
        for (int i = 0; i < n; i++) {
            for (int j = i; j > 0 && less(a[j], a[j - 1]); j--) {
                exch(a, j, j - 1);
                show(a);
            }
        }
    }
}
