package algo.ch0201;

/**
 * @author mougua
 */
public class Shell extends SortAlgorithm {
    public static void sort(Comparable[] a) {
        int n = a.length;
        int h = 1;
        while (h < n / 3) {
            h = 3 * h + 1;
        }
        while (h >= 1) {
            System.out.println("h = " +  h);
            for (int i = h; i < n; i++) {
                for (int j = i; j >= h && less(a[j], a[j - h]); j -= h) {
                    exch(a, j, j - h);
                }
                System.out.println("i = " + i);
                show(a);
            }
            h = h / 3;
            System.out.println();
        }
    }
}
