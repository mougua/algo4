package algo.ch0105;

/**
 * @author mougua
 */
public class UfQuickUnionWeighted {
    private final int[] id;
    private int[] size;
    private int count;

    public UfQuickUnionWeighted(int n) {
        count = n;
        id = new int[n];
        for (int i = 0; i < n; i++) {
            // 右边的i表示连着触点i
            id[i] = i;
        }
        size =  new int[n];
        for (int i = 0; i < n; i++) {
            size[i] = i;
        }
    }

    public void union(int p, int q) {
        int pRoot = find(p);
        int qRoot = find(q);
        if (pRoot == qRoot) {
            return;
        }
        if (size[pRoot] < size[qRoot]) {
            id[pRoot] = qRoot;
            size[qRoot] += size[pRoot];
        } else {
            id[qRoot] = pRoot;
            size[pRoot] += size[qRoot];
        }
        count--;
    }

    public int find(int p) {
        while (p != id[p]) {
            p = id[p];
        }
        return p;
    }

    public boolean connected(int p, int q) {
        return find(p) == find(q);
    }

    public int count() {
        return count;
    }
}
