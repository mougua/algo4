package algo.ch0202;

import algo.ch0201.Insertion;
import algo.ch0201.Selection;
import algo.ch0201.Shell;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class ChTest {
    @Test
    public void testMerge() {
        String[] e = {"S","H","E","L","L",
                "S","O","R","T",
                "E","X","A","M","P","L","E"};
        Merge.sort(e);
        System.out.println(Arrays.toString(e));
        Assert.assertTrue(Selection.isSorted(e));
        System.out.println("");
    }

}