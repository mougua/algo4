package algo.ch0103;

import org.junit.Test;

/**
 * Unit test for simple algo.App.
 */
public class ChTest
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testFixedCapacityStackOfStrings()
    {
        FixedCapacityStackOfStrings s = new FixedCapacityStackOfStrings(10);
        s.push("aaa");
        s.push("bbb");
        s.push("ccc");
        s.push("ddd");
        s.push("ddd");
        s.push("ddd");
        System.out.println(s.pop());
        System.out.println(s.pop());
        System.out.println(s);
    }

    @Test
    public void testFixedCapacityStack()
    {
        ResizingArrayStack<String> s = new ResizingArrayStack<>(4);
        s.push("1");
        s.push("2");
        s.push("3");
        s.push("4");
        s.push("5");
        s.push("6");
        s.push("7");
        s.push("8");
        s.push("9");
        System.out.println(s.pop());
        System.out.println(s.pop());
        System.out.println(s);

        for (String s2 : s) {
            System.out.println(s2);
        }
    }
}
