package algo.ch0201;

import org.junit.Assert;
import org.junit.Test;

public class ChTest {
    @Test
    public void testSelection() {
        Integer[] a = {5, 3, 4, 2, 1};
        Selection.sort(a);
        Assert.assertTrue(SortAlgorithm.isSorted(a));
        System.out.println("");

        String[] b = {"D", "A", "E", "M", "O", "N"};
        Selection.sort(b);
        Assert.assertTrue(SortAlgorithm.isSorted(b));
        System.out.println("");

    }

    @Test
    public void testInsertion() {
        String[] c = {"D", "A", "E", "M", "O", "N"};
        Insertion.sort(c);
        Assert.assertTrue(SortAlgorithm.isSorted(c));
        System.out.println("");

    }

    @Test
    public void testShell() {
        String[] d = {"D", "A", "E", "M", "O", "N", "S", "L", "A", "Y", "E", "R"};
        Shell.sort(d);
        Assert.assertTrue(SortAlgorithm.isSorted(d));
        System.out.println("");

        String[] e = {"S", "H", "E", "L", "L",
                "S", "O", "R", "T",
                "E", "X", "A", "M", "P", "L", "E"};
        Shell.sort(e);
        Assert.assertTrue(Selection.isSorted(e));
        System.out.println("");
    }

}